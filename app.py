import datetime

print('This is app.py!!!')

lines = []
lines.append('# CI/CD Test\n\n')
lines.append('- Update README test\n')
lines.append(f'- Updated at `{datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}`\n\n')
print('Lines :', lines)

with open('README.md', 'w', encoding='utf-8') as fp:
    fp.writelines(lines)

print('README.md has updated.')
